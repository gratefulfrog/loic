$fn=1000;

m4Dia = 4;
m3Dia = 3;

// [Big diameter, hole axis Diameter, hole axis angle]
ins_3_1_8 = [80.264,  88.9,   45];
ins_2_1_4 = [58.7375, 66.675, 45];
ins_1_1_4 = [31.75,	  39.624, 45];

insVec = [ins_3_1_8, ins_2_1_4,ins_1_1_4];


module instrumentTemplate(dimVec,mountingHoleDia){
  bigDia      = dimVec[0];
  holeAxisRad = dimVec[1]/2;
  alpha       = dimVec[2];
  mountingPoints = [[0,holeAxisRad,0],
                    [holeAxisRad,0,0],
                    [0,-holeAxisRad,0],
                    [-holeAxisRad,0,0]];
  circle(d=bigDia);
  rotate([0,0,alpha])
    for (p=mountingPoints)
      translate(p)
        circle(d=mountingHoleDia);
    
}

for (i=[0:2])
  translate([100*i,0,0])
    instrumentTemplate(insVec[i],m4Dia);